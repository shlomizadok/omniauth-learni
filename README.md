# OmniAuth Learni OAuth2 Strategy

Strategy to auth with Learni via OAuth2 in OmniAuth.

## Installation

To use the strategy you must first add it to your `Gemfile`:

    :::ruby
    gem 'omniauth-learni', :git => 'https://bitbucket.org/nimast/omniauth-learni.git'
    
Then add it to your `omniauth` initializer file like so:

	:::ruby
	Rails.application.config.middleware.use OmniAuth::Builder do
		...
		provider :learni, YOUR_APP_KEY, YOUR_APP_SECRET
		...
	end
	
## Configuration

You can set up the address of the specific authbox you want to authenticate with. This can be done by passing a lambda to the provider delaration setting the `site` client option to the desired authbox url, for example:

	:::ruby
	provider :learni, YOUR_APP_KEY, YOUR_APP_SECRET,
             :setup => lambda { |env| env['omniauth.strategy'].options[:client_options][:site] = Settings.learni_authbox_url }

## License

Copyright (c) 2012 by Learni LTD

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.