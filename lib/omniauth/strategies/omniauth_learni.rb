module OmniAuth
  module Strategies
    class Learni < OmniAuth::Strategies::OAuth2

      option :name, :learni

      option :client_options, {
        :site => "https://learni-authbox.herokuapp.com",
        :authorize_url => "/oauth/authorize"
      }

      uid { raw_info["id"] }

      info do
        {
          :email => raw_info["email"],
          :created_at => raw_info["created_at"],
          :updated_at => raw_info["updated_at"]
        }
      end

      def raw_info
        @raw_info ||= access_token.get('/api/v1/me.json').parsed
      end

    end
  end
end